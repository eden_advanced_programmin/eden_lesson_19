﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lesson_19
{
    public partial class birthday : Form
    {
        public birthday()
        {
            InitializeComponent();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void search_Click(object sender, EventArgs e)
        {
            string date_to_search = monthCalendar1.SelectionStart.Month.ToString() + '/' +
                monthCalendar1.SelectionStart.Day.ToString() + '/' + monthCalendar1.SelectionStart.Year.ToString();

            //open file of the user
            string file_name = @"C:\Users\magshimim\source\repos\lesson_19\lesson_19\" + Username + "BD.txt";
            System.IO.StreamReader file = new System.IO.StreamReader(file_name);
            string line;
            int flag = 0;

            //compare the date to "UserDB.txt"
            while ((line = file.ReadLine()) != null)
            {
                //strings_file[0] = name , strings_file[1] = date
                String[] strings_file = line.Split(',');
                if (date_to_search == strings_file[1])
                {
                    flag = 1;
                    this.label2.Text = strings_file[0] + " has a birthday on the selected date";
                }
            }
            file.Close();

            if (flag == 0) //if username or password is incorrect
            {
                this.label2.Text = "No one has a birthday on the selected date";
            }

            
        }
        
    }
}